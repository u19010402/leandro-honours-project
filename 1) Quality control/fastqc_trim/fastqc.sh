#!/bin/bash
#PBS -l nodes=1:ppn=24
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -o /nlustre/users/leandro/2022/hons_project/fastqc/fastqc_trim
#PBS -e /nlustre/users/leandro/2022/hons_project/fastqc/fastqc_trim
#PBS -k oe
#PBS -m ae
#PBS -M email@tuks.co.za

module load fastqc-0.11.7

cd /nlustre/users/leandro/2022/hons_project/trim

for i in QY*
do
        fastqc $i -o /nlustre/users/leandro/2022/hons_project/fastqc/fastqc_trim

done
