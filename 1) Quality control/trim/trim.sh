#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/trim
#PBS -e /nlustre/users/leandro/2022/hons_project/trim
#PBS -M email@tuks.co.za

export JAVA_OPTS="-Xmx2g"
module load trimmomatic-0.36

java -jar $TRIMMOMATIC PE /nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ/QY007_1.fastq.gz \
/nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ/QY007_2.fastq.gz \
/nlustre/users/leandro/2022/hons_project/trim/QY007_1_trim_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/QY007_1_trim_singles.fastq.gz \
/nlustre/users/leandro/2022/hons_project/trim/QY007_2_trim_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/QY007_2_trim_singles.fastq.gz \
LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:50 \
ILLUMINACLIP:/apps/trimmomatic-0.36/adapters/TruSeq3-PE-2.fa:2:30:10

java -jar $TRIMMOMATIC PE /nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ/QY008_1.fastq.gz \
/nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ/QY008_2.fastq.gz \
/nlustre/users/leandro/2022/hons_project/trim/QY008_1_trim_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/QY008_1_trim_singles.fastq.gz \
/nlustre/users/leandro/2022/hons_project/trim/QY008_2_trim_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/QY008_2_trim_singles.fastq.gz \
LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:50 \
ILLUMINACLIP:/apps/trimmomatic-0.36/adapters/TruSeq3-PE-2.fa:2:30:10

java -jar $TRIMMOMATIC PE /nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ/QY009_1.fastq.gz \
/nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ/QY009_2.fastq.gz \
/nlustre/users/leandro/2022/hons_project/trim/QY009_1_trim_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/QY009_1_trim_singles.fastq.gz \
/nlustre/users/leandro/2022/hons_project/trim/QY009_2_trim_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/QY009_2_trim_singles.fastq.gz \
LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:50 \
ILLUMINACLIP:/apps/trimmomatic-0.36/adapters/TruSeq3-PE-2.fa:2:30:10
