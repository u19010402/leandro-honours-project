#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -o /nlustre/users/leandro/2022/hons_project/fastqc/fastqc_raw
#PBS -e /nlustre/users/leandro/2022/hons_project/fastqc/fastqc_raw
#PBS -k oe
#PBS -m ae
#PBS -M email@tuks.co.za

module load fastqc-0.11.7

cd /nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ

for i in QY*
do
	fastqc $i -o /nlustre/users/leandro/2022/hons_project/haplogenomes/fastqc/fastqc_raw

done
