#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/alignment/grandis_index
#PBS -e /nlustre/users/leandro/2022/hons_project/alignment/grandis_index
#PBS -M email@tuks.co.za

module load hisat2-2.1.0

cd /nlustre/users/leandro/2022/hons_project/alignment/grandis_index

hisat2-build -p 28 /nlustre/users/leandro/2022/hons_project/repeatmasker/grandis/grandis_anneri.fasta.masked Grandis_masked
