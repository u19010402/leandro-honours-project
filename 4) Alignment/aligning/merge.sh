#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/alignment/scripts
#PBS -e /nlustre/users/leandro/2022/hons_project/alignment/scripts
#PBS -M email@tuks.co.za

module load samtools-1.10

cd /nlustre/users/leandro/2022/hons_project/alignment/

samtools merge merged_grandis.bam QY007_grandis_sorted.bam QY008_grandis_sorted.bam QY009_grandis_sorted.bam

samtools merge merged_urophylla.bam QY007_urophylla_sorted.bam  QY008_urophylla_sorted.bam  QY009_urophylla_sorted.bam

samtools index merged_grandis.bam

echo 'GRANDIS DONE'

samtools index merged_urophylla.bam

echo 'UROPHYLLA DONE'
