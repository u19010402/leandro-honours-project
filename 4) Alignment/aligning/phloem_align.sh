#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/alignment/scripts
#PBS -e /nlustre/users/leandro/2022/hons_project/alignment/scripts
#PBS -M email@tuks.co.za

module load hisat2-2.1.0

cd /nlustre/users/leandro/2022/hons_project/alignment

hisat2 -x /nlustre/users/leandro/2022/hons_project/alignment/grandis_index/Grandis_masked \
        -1 /nlustre/users/leandro/2022/hons_project/split_results/QY008_x_1.fq -2 /nlustre/users/leandro/2022/hons_project/split_results/QY008_x_2.fq \
        -p 28 \
        -S /nlustre/users/leandro/2022/hons_project/alignment/QY008_grandis.sam 


hisat2 -x /nlustre/users/leandro/2022/hons_project/alignment/urophylla_index/Urophylla_masked \
        -1 /nlustre/users/leandro/2022/hons_project/split_results/QY008_y_1.fq -2 /nlustre/users/leandro/2022/hons_project/split_results/QY008_y_2.fq \
        -p 28 \
        -S /nlustre/users/leandro/2022/hons_project/alignment/QY008_urophylla.sam 


module unload hisat2-2.2.1

module load samtools-1.10 

samtools view -@ 28 -uhS /nlustre/users/leandro/2022/hons_project/alignment/QY008_grandis.sam | samtools sort -@ 28 -T /nlustre/users/leandro/2022/hons_project/alignment/QY008_grandis  -o /nlustre/users/leandro/2022/hons_project/alignment/QY008_grandis_sorted.bam
samtools view -@ 28 -uhS /nlustre/users/leandro/2022/hons_project/alignment/QY008_urophylla.sam | samtools sort -@ 28 -T /nlustre/users/leandro/2022/hons_project/alignment/QY008_urophylla -o /nlustre/users/leandro/2022/hons_project/alignment/QY008_urophylla_sorted.bam  


echo "QY008_grandis_sorted.bam"
samtools flagstat -@ 28 /nlustre/users/leandro/2022/hons_project/alignment/QY008_grandis_sorted.bam

echo "QY008_urophylla_sorted.bam"
samtools flagstat -@ 28 /nlustre/users/leandro/2022/hons_project/alignment/QY008_urophylla_sorted.bam

done
