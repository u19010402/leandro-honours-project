#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_2/filtering/test
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_2/filtering/test
#PBS -M email@tuks.co.za

cd /nlustre/users/leandro/2022/hons_project/braker_2/filtering/test

module load perl-5.26.1

genome="/nlustre/users/leandro/2022/hons_project/repeatmasker/grandis/grandis_anneri.fasta.masked"
alignment="/nlustre/users/leandro/2022/hons_project/braker_2/grandis/braker/augustus.hints.gff3"
script="/apps/gFACs/gFACs.pl"

if [ ! -d gra_test_mono_o ]; then
        mkdir gra_test_mono_o 
fi
if [ ! -d gra_test_multi_o ]; then
        mkdir gra_test_multi_o
fi

perl "$script" \
        -f braker_2.05_gff3 \
        --statistics \
        --statistics-at-every-step \
        --splice-table \
        --unique-genes-only \
        --rem-multiexonics \
        --rem-all-incompletes \
        --rem-genes-without-start-codon \
        --rem-genes-without-stop-codon \
        --get-protein-fasta \
        --fasta "$genome" \
        -O gra_test_mono_o \
        "$alignment" 

echo 'MONO COMPLETE'

perl "$script" \
        -f braker_2.05_gff3 \
        --statistics \
        --statistics-at-every-step \
        --splice-table \
        --unique-genes-only \
        --rem-monoexonics \
	--min-exon-size 3 \
	--rem-all-incompletes \
        --rem-genes-without-start-codon \
        --rem-genes-without-stop-codon \
        --get-protein-fasta \
        --fasta "$genome" \
        -O gra_test_multi_o \
        "$alignment"

echo 'MULTI COMPLETE'
