#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_2/filtering/test/gra_final_o/
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_2/filtering/test/gra_final_o/
#PBS -M email@tuks.co.za

module load busco-5.3.2

#AUG_HOME=$HOME/Augustus
#export AUGUSTUS_CONFIG_PATH=${AUG_HOME}/config

cd /nlustre/users/leandro/2022/hons_project/braker_2/filtering/test/gra_final_o/

busco -f -i genes.fasta.faa \
        -o gra_busco \
        -c 28 \
        -l /apps/busco-5.3.2/busco_downloads/lineages/embryophyta_odb10 -m prot \
        --offline
