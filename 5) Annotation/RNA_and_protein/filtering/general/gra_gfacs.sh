#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_2/filtering/general
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_2/filtering/general
#PBS -M email@tuks.co.za

cd /nlustre/users/leandro/2022/hons_project/braker_2/filtering/general

module load perl-5.26.1

genome="/nlustre/users/leandro/2022/hons_project/repeatmasker/grandis/grandis_anneri.fasta.masked"
alignment="/nlustre/users/leandro/2022/hons_project/braker_2/grandis/braker/augustus.hints.gff3"
script="/apps/gFACs/gFACs.pl"

if [ ! -d gra_general ]; then
        mkdir gra_general
fi


perl "$script" \
        -f braker_2.05_gff3 \
        --statistics \
        --splice-table \
        --get-protein-fasta \
        --fasta "$genome" \
        -O gra_general \
        "$alignment"
