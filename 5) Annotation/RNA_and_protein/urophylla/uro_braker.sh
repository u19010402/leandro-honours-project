#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=900:00:00
#PBS -q long  
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_2           
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_2           
#PBS -M email@tuks.co.za

module load braker-2.1.6

cd /nlustre/users/leandro/2022/hons_project/braker_2/urophylla

BAM=/nlustre/users/leandro/2022/hons_project/alignment/merged_urophylla.bam
GENOME=/nlustre/users/leandro/2022/hons_project/repeatmasker/urophylla/urophylla_anneri.fasta.masked

braker.pl --genome=${GENOME} \
        --bam ${BAM} \
        --AUGUSTUS_ab_initio \
        --softmasking 1 \
	--prot_seq=/nlustre/users/karen/from_galaxy_data/Eucalyptus/EucalyptusGrandis/Genome/v2.0/annotation/Egrandis_297_v2.0.protein.fa \
	--prg=gth --gth2traingenes \
        --gff3 \
        --cores 28


