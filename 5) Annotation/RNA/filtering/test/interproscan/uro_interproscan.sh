#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test/uro_test_mono_o/interproscan
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test/uro_test_mono_o/interproscan
#PBS -M email@tuks.co.za

module load interproscan-5.29

cd /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test/uro_test_mono_o/interproscan

sed 's/*$//g' ../genes.fasta.faa > genes.fasta.faa

interproscan.sh -appl Pfam -i genes.fasta.faa

