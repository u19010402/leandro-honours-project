#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=900:00:00
#PBS -q long
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test
#PBS -M email@tuks.co.za

module load perl-5.26.1

genome="/nlustre/users/leandro/2022/hons_project/repeatmasker/grandis/grandis_anneri.fasta.masked"
alignment="/nlustre/users/leandro/2022/hons_project/braker_1/grandis/braker/augustus.hints.gff3"
script="/apps/gFACs/gFACs.pl"

cd /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test/gra_test_mono_o/

sed 's/\s.*$//' interproscan/genes.fasta.faa.tsv | uniq > gra_pfam_ids.txt

python /nlustre/users/leandro/2022/hons_project/braker_1/filtering/filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList gra_pfam_ids.txt  --idPath . --out gra_pfam_gene_table.txt

cd /nlustre/users/leandro/2022/hons_project/braker_1/filtering/test/gra_test_multi_o
# remove lines with 5_INC+3_INC here
grep '5_INC+3_INC' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > fullinc_ids.txt
grep 'gene' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > gene_table_ids.txt
grep -vxFf fullinc_ids.txt gene_table_ids.txt > comp_inc_ids.txt
python /nlustre/users/leandro/2022/hons_project/braker_1/filtering/filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList comp_inc_ids.txt --idPath . --out comp_inc_gene_table.txt

if [ ! -d gra_start_o ]; then
	mkdir gra_start_o
fi

if [ ! -d gra_stop_o ]; then
	mkdir gra_stop_o
fi

alignment="comp_inc_gene_table.txt"

perl "$script" \
	-f gFACs_gene_table \
	--no-processing \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--rem-genes-without-start-codon \
	--fasta "$genome" \
	-O gra_start_o \
	"$alignment"

perl "$script" \
	-f gFACs_gene_table \
	--no-processing \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--rem-genes-without-stop-codon \
	--fasta "$genome" \
	-O gra_stop_o \
	"$alignment"

grep 'gene' gra_start_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' | uniq > starts_ids.txt
grep 'gene' gra_stop_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' |  uniq > stops_ids.txt
grep -vxFf starts_ids.txt comp_inc_ids.txt > missing_starts_ids.txt
grep -vxFf stops_ids.txt comp_inc_ids.txt > missing_stops_ids.txt
grep -xFf missing_starts_ids.txt missing_stops_ids.txt > missing_both_ids.txt
grep -vxFf missing_both_ids.txt comp_inc_ids.txt > completes_partials_ids.txt

python /nlustre/users/leandro/2022/hons_project/braker_1/filtering/filtergFACsGeneTable.py --table comp_inc_gene_table.txt --tablePath . --idList completes_partials_ids.txt --idPath . --out completes_partials_gene_table.txt

cd ../

cat gra_test_mono_o/gra_pfam_gene_table.txt  gra_test_multi_o/gene_table.txt > gra_mono_multi_gene_table.txt

if [ ! -d gra_final_o ]; then
        mkdir gra_final_o
fi

alignment="gra_mono_multi_gene_table.txt"

perl "$script" \
	-f gFACs_gene_table \
	--no-processing \
	--statistics \
	--splice-table \
	--get-protein-fasta \
	--create-gff3 \
	--create-gtf \
	--fasta "$genome" \
	-O gra_final_o \
	"$alignment"






