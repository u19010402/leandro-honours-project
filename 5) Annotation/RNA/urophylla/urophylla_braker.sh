#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=900:00:00
#PBS -q long  
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/braker_1           
#PBS -e /nlustre/users/leandro/2022/hons_project/braker_1         
#PBS -M email@tuks.co.za

module load braker-2.1.6

cd /nlustre/users/leandro/2022/hons_project/braker_1/urophylla

BAM=/nlustre/users/leandro/2022/hons_project/alignment/merged_urophylla.bam
GENOME=/nlustre/users/leandro/2022/hons_project/repeatmasker/urophylla/urophylla_anneri.fasta.masked

braker.pl --genome=${GENOME} \
        --bam ${BAM} \
	--AUGUSTUS_ab_initio \
        --softmasking 1 \
        --gff3 \
        --cores 28

