# Structural annotation of phased parental haplogenomes through RNA sequencing of a _Eucalyptus_ F1 hybrid progeny tree
Submitted in partial fulfilment of the degree: 
BSc (Hons) Bioinformatics

Department of Biochemistry, Genetics and Microbiology

University of Pretoria
## Aim 
The aim of the study was to determine the best method of using RNA-seq short reads from an interspecific F1 hybrid for structural annotation of the _E. urophylla_ and _E. grandis_ parental haplogenomes contained in the F1 hybrid. I also aimed to determine the frequency of sequence variants in the unbinned reads vs those that could not be assigned uniquely to the parental haplogenomes.

## Tissue sampling and RNA sequencing
Xylem, phloem and leaf tissue from a _E. urophylla_ x _E. grandis_ F1 hybrid, grown in Zululand (Sappi Forest Research, Kwambonambi, KZN), was collected and flash frozen in liquid nitrogen to preserve RNA. The tissues were shipped to Macrogen (Macrogen Inc., Seoul, South Korea) for RNA extraction and sequencing. RNA was extracted using the Invitrogen PureLink Plant RNA Reagent kit. Following RNA extraction, a TruSeq stranded total RNA with Ribo-Zero Plant library was constructed. Total RNA was sequenced using the NovaSeq 6000 S4, generating 150 bp paired-end reads.

## Quality control
**FastQC** was used to assess the quality of the reads before and after trimming. To remove bases with Phred quality scores below 30 at the start and end of a read, reads were trimmed with **Trimmomatic** to a minimum length of 50 bases. A sliding window approach was used, with a window size of 4 and the average Phred quality score required to retain a read was 20. Only surviving paired reads were used going forward.

## Repeat masking
Reference haplogenomes for _E. urophylla_ and _E. grandis_ (previously assembled by Ms Julia Candotti for the same F1 hybrid individuals) underwent repeat masking via **RepeatModeler** and **RepeatMasker**. 

**RepeatModeler** was used to create libraries containing repeat elements for each haplogenome. Repetitive regions within the genome were masked with **RepeatMasker** through alignment of repeat elements found in the **RepeatModeler** libraries to the relevant haplogenome sequence. For this study, both parental haplogenomes underwent soft-masking, as annotation with unmasked genomes can lead to the overestimation of the number of genes predicted.

## Binning
Binning is the process of assigning reads to the reference with which it aligns the best. The RNA-seq reads from each tissue in the F1 hybrid were mapped to the masked parental haplogenomes in order to assign the reads to the parent of origin. Mapping was done via **BBSplit**, a module embedded in **BBMap**.

## Alignment
Masked haplogenomes were indexed and binned reads from each tissue were aligned to their respective haplogenomes with **HISAT2** under default parameters. Resulting SAM files were sorted according to chromosome name and converted to BAM files with **samtools**. BAM files from each tissue were merged into a single BAM file containing all sorted alignments for _E. grandis_ and _E. urophylla_, respectively.

## Annotation
**BRAKER2** was employed to predict structural gene models. It is a pipeline that employs **GeneMark-EP**, and **AUGUSTUS**. **GeneMark-EP** is an unsupervised machine learning tool which predicts gene models. These gene models are used by **AUGUSTUS**, a supervised machine learning tool, along with extrinisic evidence to predict genes. 

The collaboration of these two tools through **BRAKER2** guides the identification and prediction of genes within each haplogenome. **BUSCO** was used to assess the completeness of each annotation using the Embryophyta database.

#### RNA evidence
The first strategy made use of only the merged RNA alignment as input for **BRAKER2** to annotate each respective haplogenome.

#### RNA and protein evidence
The second procedure employed RNA and protein evidence. Protein sequences were collected from the v2.0 _E. grandis_ reference annotation. The
respective BAM files and the protein FASTA file from the v2.0 annotation was used as evidence for both _E. grandis_ and _E. urophylla_ haplogenome annotation.

#### Filtering
**gFACs** was used to filter for viable genes in each annotation. Genes that were 5’ and 3’ incomplete were removed, genes without a start or stop codon were removed, genes without an in-frame stop codon were removed, and only unique isoforms (longest transcripts) were not filtered. After this, mono-exonic genes were filtered with **InterProScan** and those which contain protein domains were kept. 

The final mono- and multi-exonic genes were combined into a single annotation and these genes were translated to protein sequences. **BUSCO** was used to assess the completeness of each annotation by using the protein sequences as input.

## Variant calling
In order to determine the frequency of SNPs and INDELs in the ambiguous and total (unbinned) reads variant calling was performed four times. The first two times called SNPs and INDELs in the total binned reads by aligning against both the _E. grandis_ and _E. urophylla_ haplogenome. The third and fourth times called variants on only the ambiguous reads and their alignments against the _E. grandis_ and _E. urophylla_ haplogenomes. The method used for calling variants is described below.

#### Pre-processing
Trimmed reads from all three tissues were aligned to each haplogenome using the 2-pass method with **STAR** as recommended by GATK’s variant calling pipeline. The 2-pass method involves a single normal alignment with **STAR**, after which the splice junctions discovered from the first alignment are used as ‘annotated’ junctions in the second alignment. This improves the alignments around novel splice junctions. 

This resulted in BAM files for total reads uniquely aligned to the i) _E. grandis_, or ii) _E. urophylla_ haplogenome and (ambiguous) reads aligned to both the iii) _E. grandis_ and iv) _E. urophylla_ haplogenomes. **Picard** was used to sort the alignment files and mark duplicate reads which correspond to sequencing/PCR artifacts. BED files were created for each of these BAM files with **BEDtools**.

#### SNP & INDEL calling
Three tools contained within the GATK suite were used namely; **SplitNCigarReads**, **HaplotypeCaller** and **SelectVariants**. **SplitNCigarReads** was used to reformat the RNA alignments for **HaplotypeCaller** to function since RNA aligners and DNA aligners have different conventions. **HaplotypeCaller** was used to call short variants and **SelectVariants** was used to split the variants found into SNPs and INDELs.

#### Filtering
SNPs and INDELs were filtered through GATK’s **VariantFiltration** tool. Filters used were identical to GATK’s recommended hard-filters. The metrics used for hard-filtering are; 

i) QualByDepth (QD), this metric reflects the variant confidence and is used to normalize variant quality where deep coverage would otherwise inflate variant confidence, 

ii) FisherStrand (FS), this metric is used to inform whether there is a strand bias with a variant seen with different frequencies on the forward and reverse reads strands, a value of zero for this metric indicates there is no strand bias, 

iii) StrandOddsRatio (SOR), is another metric used to assess strand bias, it focuses on the reads covering the ends of exons as these are often only covered in a single direction, and 

iv) ReadPosRankSumTest (ReadPosRankSum), this metric assesses the positions of reference and variants in the reads, if the position occurs near   the end of reads this can be due to errors during sequencing.

SNPs were filtered with; i) QD lower than 2, ii) FS higher than 60, iii) SOR above 3 and iv) ReadPosRankSum less than -8. INDELs were filtered with; i) QD lower than 2, ii) FS above 200 and iii) ReadPosRankSum less than -8.

#### Variant analysis
**VCFtools** was used to calculate the number of variants across each chromosome. SNP and INDEL files were intersected with relevant BED files in order include only specific sites to which the reads aligned. The frequency each variant was calculated by dividing the number of variants in each chromosome across the total sequence length for that chromosome in the relevant BED files. **R** was used to plot the frequency of each variant against each haplogenome.

For example, to calculate the frequency of SNPs in the total reads against the E. grandis haplogenome, **VCFtools** was used to calculate the number of SNPs per chromosome, and this was intersected with the BED file containing the sites to which the total reads aligned to. The sequence length of each chromosome was retrieved from the BED file corresponding to the alignment of the total reads to the E. grandis haplogenome. The number of SNPs for each chromosome was divided by the length that chromosome.

## Project data
All project data is located at University of Pretoria, Centre for Bioinformatics and Computational Biology.
`@wonko.bi.up.ac.za`

**Complete project:** `/nlustre/users/leandro/2022`
- **Annotation section:** `/nlustre/users/leandro/2022/hons_project`
- **Variant calling section:** `/nlustre/users/leandro/2022/fresh`

**Raw RNA short-read data:** `/nlustre/users/leandro/2022/hons_project/data/RNA_Seq_2022/RawFASTQ`

**Haplogenome assemblies:** `/nlustre/users/leandro/2022/hons_project/data/haplogenomes`

**Pre-filter haplogenome annotations:** `/nlustre/users/leandro/2022/hons_project/pre_filter_annotations`

**Post-filter haplogenome annotations:** `/nlustre/users/leandro/2022/hons_project/post_filter_annotations`
