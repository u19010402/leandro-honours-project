#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -o /nlustre/users/leandro/2022/hons_project/split_index/
#PBS -e /nlustre/users/leandro/2022/hons_project/split_index/
#PBS -k oe
#PBS -m ae
#PBS -M email@tuks.co.za

module load bbmap

bbsplit.sh path=/nlustre/users/leandro/2022/hons_project/split_index/ build=3 ref_x=/nlustre/users/leandro/2022/hons_project/repeatmasker/grandis/grandis_anneri.fasta.masked ref_y=/nlustre/users/leandro/2022/hons_project/repeatmasker/urophylla/urophylla_anneri.fasta.masked
