#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -o /nlustre/users/leandro/2022/hons_project/split_results/
#PBS -e /nlustre/users/leandro/2022/hons_project/split_results/
#PBS -k oe
#PBS -m ae
#PBS -M email@tuks.co.za

module load bbmap

#create index first

bbsplit.sh path=/nlustre/users/leandro/2022/hons_project/split_index/ build=3 in=/nlustre/users/leandro/2022/hons_project/trim/QY008_1_trim_paired.fastq.gz in2=/nlustre/users/leandro/2022/hons_project/trim/QY008_2_trim_paired.fastq.gz maxindel=1600  ambiguous2=all basename=/nlustre/users/leandro/2022/hons_project/split_results/QY008_%_#.fq scafstats=/nlustre/users/leandro/2022/hons_project/split_results/QY008_scafstats.txt refstats=/nlustre/users/leandro/2022/hons_project/split_results/QY008_refstats.txt
