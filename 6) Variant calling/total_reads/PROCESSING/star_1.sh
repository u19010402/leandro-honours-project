#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/STAR/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/STAR/scripts
#PBS -M u19010402@tuks.co.za

module load STAR

cd /nlustre/users/leandro/2022/fresh/REDO/STAR/gra_alignment

STAR --runThreadN 28 --readFilesCommand zcat --genomeDir /nlustre/users/leandro/2022/fresh/STAR/gra_index --readFilesIn /nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY007_1_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY008_1_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY009_1_trimV2_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY007_2_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY008_2_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY009_2_trimV2_paired.fastq.gz --outSAMattrRGline ID:007 SM:007 , ID:008 SM:008 , ID:009 SM:009 --outFileNamePrefix /nlustre/users/leandro/2022/fresh/REDO/STAR/gra_alignment/gra_align

#cd /nlustre/users/leandro/2022/fresh/REDO/STAR/uro_alignment

#STAR --runThreadN 28 --readFilesCommand zcat --genomeDir /nlustre/users/leandro/2022/fresh/STAR/uro_index --readFilesIn /nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY007_1_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY008_1_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY009_1_trimV2_paired.fastq.gz /nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY007_2_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY008_2_trimV2_paired.fastq.gz,/nlustre/users/leandro/2022/hons_project/trim/trim_v2/QY009_2_trimV2_paired.fastq.gz --outSAMattrRGline ID:007 , ID:008 , ID:009 --outFileNamePrefix /nlustre/users/leandro/2022/fresh/REDO/STAR/uro_alignment/uro_align




