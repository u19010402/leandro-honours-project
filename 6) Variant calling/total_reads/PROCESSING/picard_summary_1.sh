#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/STAR/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/STAR/scripts
#PBS -M u19010402@tuks.co.za

module load picard-2.17.11

cd /nlustre/users/leandro/2022/fresh/REDO/STAR/2nd_gra_alignment

java -jar $PICARD CollectAlignmentSummaryMetrics R=/nlustre/users/leandro/2022/hons_project/data/haplogenomes/grandis_anneri.fasta I=sorted_reads.bam O=alignment_metrics.txt

#cd /nlustre/users/leandro/2022/fresh/REDO/STAR/2nd_uro_alignment

#java -jar $PICARD CollectAlignmentSummaryMetrics R=/nlustre/users/leandro/2022/hons_project/data/haplogenomes/urophylla_anneri.fasta I=sorted_reads.bam O=alignment_metrics.txt
