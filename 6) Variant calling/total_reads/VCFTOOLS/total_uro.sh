#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/VCFTOOLS/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/VCFTOOLS/scripts
#PBS -M email@tuks.co.za

module load vcf-tools

cd /nlustre/users/leandro/2022/fresh/REDO/VCFTOOLS/urophylla

vcftools --vcf /nlustre/users/leandro/2022/fresh/REDO/GATK/urophylla/uro_snps_filtered.vcf --bed /nlustre/users/leandro/2022/fresh/REDO/PROCESSING/duplication/urophylla/urophylla_merged.bed --remove-filtered-all --out uro_test_snp --site-depth

uro_test_snp.ldepth | grep -w chr1 | wc -l
cat uro_test_snp.ldepth | grep -w chr1 | wc -l
cat uro_test_snp.ldepth | grep -w chr2 | wc -l
cat uro_test_snp.ldepth | grep -w chr3 | wc -l
cat uro_test_snp.ldepth | grep -w chr4 | wc -l
cat uro_test_snp.ldepth | grep -w chr5 | wc -l
cat uro_test_snp.ldepth | grep -w chr6 | wc -l
cat uro_test_snp.ldepth | grep -w chr7 | wc -l
cat uro_test_snp.ldepth | grep -w chr8 | wc -l
cat uro_test_snp.ldepth | grep -w chr9 | wc -l
cat uro_test_snp.ldepth | grep -w chr10 | wc -l
cat uro_test_snp.ldepth | grep -w chr11 | wc -l

vcftools --vcf /nlustre/users/leandro/2022/fresh/REDO/GATK/urophylla/uro_indels_filtered.vcf --bed /nlustre/users/leandro/2022/fresh/REDO/PROCESSING/duplication/urophylla/urophylla_merged.bed --remove-filtered-all --out uro_test_indel --site-depth

uro_test_indel.ldepth | grep -w chr1 | wc -l
cat uro_test_indel.ldepth | grep -w chr1 | wc -l
cat uro_test_indel.ldepth | grep -w chr2 | wc -l
cat uro_test_indel.ldepth | grep -w chr3 | wc -l
cat uro_test_indel.ldepth | grep -w chr4 | wc -l
cat uro_test_indel.ldepth | grep -w chr5 | wc -l
cat uro_test_indel.ldepth | grep -w chr6 | wc -l
cat uro_test_indel.ldepth | grep -w chr7 | wc -l
cat uro_test_indel.ldepth | grep -w chr8 | wc -l
cat uro_test_indel.ldepth | grep -w chr9 | wc -l
cat uro_test_indel.ldepth | grep -w chr10 | wc -l
cat uro_test_indel.ldepth | grep -w chr11 | wc -l
