#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -M email@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/GATK/grandis

gatk SplitNCigarReads -R /nlustre/users/leandro/2022/hons_project/data/grandis_anneri.fasta -I /nlustre/users/leandro/2022/fresh/REDO/PROCESSING/duplication/grandis/dedup_reads.bam -O grandis_SNCR.bam

