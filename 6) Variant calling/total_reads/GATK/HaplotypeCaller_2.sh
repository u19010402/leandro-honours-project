#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=900:00:00
#PBS -q long
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -M email@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/GATK/urophylla

gatk HaplotypeCaller \
        -R /nlustre/users/leandro/2022/hons_project/data/urophylla_anneri.fasta \
        -I urophylla_SNCR.bam \
        -O urophylla_output.vcf.gz \
        -bamout urophylla_bamout.bam

echo "URO_DONE"
