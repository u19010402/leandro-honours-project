#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=900:00:00
#PBS -q long
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -M email@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/GATK/grandis

gatk HaplotypeCaller \
	-R /nlustre/users/leandro/2022/hons_project/data/grandis_anneri.fasta \
	-I grandis_SNCR.bam \
	-O grandis_output.vcf.gz \
	-bamout grandis_bamout.bam

echo "GRA_DONE"
