#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=900:00:00
#PBS -q long
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/GATK/scripts
#PBS -M email@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/GATK/grandis

gatk SelectVariants \
	-R /nlustre/users/leandro/2022/hons_project/data/grandis_anneri.fasta \
	-V grandis_output.vcf.gz \
	--select-type-to-include SNP \
	-O gra_raw_snp.vcf

echo 'SNP DONE'

gatk SelectVariants \
        -R /nlustre/users/leandro/2022/hons_project/data/grandis_anneri.fasta \
        -V grandis_output.vcf.gz \
        --select-type-to-include INDEL \
        -O gra_raw_indel.vcf

echo 'INDEL DONE'
