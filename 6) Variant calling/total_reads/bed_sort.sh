#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh
#PBS -e /nlustre/users/leandro/2022/fresh
#PBS -M email@tuks.co.za

module load bedtools-2.28.0

cd /nlustre/users/leandro/2022/fresh/REDO/PROCESSING/duplication/grandis

sort -k1,1 -k2,2n grandis.bed | bedtools merge > grandis_merged.bed

echo '1'

cd /nlustre/users/leandro/2022/fresh/REDO/PROCESSING/duplication/urophylla

sort -k1,1 -k2,2n urophylla.bed | bedtools merge > urophylla_merged.bed

echo '2'

