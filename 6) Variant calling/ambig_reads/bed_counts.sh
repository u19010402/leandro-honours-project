#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh
#PBS -e /nlustre/users/leandro/2022/fresh
#PBS -M email@tuks.co.za

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/duplication/grandis

awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}' gra_ambig_merged.bed
cat gra_ambig_merged.bed | grep -w | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr1 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr2 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr3 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr4 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr5 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr6 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr7 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr8 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr9 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr10 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat gra_ambig_merged.bed | grep -w chr11 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/duplication/urophylla

awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}' uro_ambig_merged.bed
cat uro_ambig_merged.bed | grep -w chr1 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr2 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr3 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr4 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr5 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr6 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr7 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr8 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr9 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr10 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'
cat uro_ambig_merged.bed | grep -w chr11 | awk -F'\t' 'BEGIN{SUM=0}{ SUM+=$3-$2 }END{print SUM}'

