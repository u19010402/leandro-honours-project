#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/scripts
#PBS -M u19010402@tuks.co.za

module load STAR

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/uro_alignment

STAR --runThreadN 28 --genomeDir /nlustre/users/leandro/2022/fresh/STAR/uro_index --readFilesIn /nlustre/users/leandro/2022/hons_project/split_results/ambig_results/AMBIGUOUS_split_QY007_y_1.fq,/nlustre/users/leandro/2022/hons_project/split_results/ambig_results/AMBIGUOUS_split_QY008_y_1.fq,/nlustre/users/leandro/2022/hons_project/split_results/ambig_results/AMBIGUOUS_split_QY009_y_1.fq /nlustre/users/leandro/2022/hons_project/split_results/ambig_results/AMBIGUOUS_split_QY007_y_2.fq,/nlustre/users/leandro/2022/hons_project/split_results/ambig_results/AMBIGUOUS_split_QY008_y_2.fq,/nlustre/users/leandro/2022/hons_project/split_results/ambig_results/AMBIGUOUS_split_QY009_y_2.fq --outSAMattrRGline ID:007 SM:007 , ID:008 SM:008 , ID:009 SM:009 --outFileNamePrefix /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/uro_alignment/uro_align
