#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/scripts
#PBS -M u19010402@tuks.co.za

module load bedtools-2.28.0

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/duplication/grandis

bedtools bamtobed -i dedup_reads.bam > ambig_grandis.bed

echo 'GRA DONE'

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/duplication/urophylla

bedtools bamtobed -i dedup_reads.bam > ambig_urophylla.bed

echo 'URO DONE'
