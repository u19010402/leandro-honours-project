#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/scripts
#PBS -M u19010402@tuks.co.za

module load picard-2.17.11

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/duplication/urophylla

java -jar $PICARD MarkDuplicates I=/nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/2nd_uro_alignment/sorted_reads.bam O=dedup_reads.bam M=dup_metrics.txt
