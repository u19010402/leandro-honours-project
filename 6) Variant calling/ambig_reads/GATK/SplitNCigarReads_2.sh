#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/scripts
#PBS -M u19010402@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/urophylla

gatk SplitNCigarReads -R /nlustre/users/leandro/2022/hons_project/data/urophylla_anneri.fasta -I /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/PROCESSING/duplication/urophylla/dedup_reads.bam -O urophylla_SNCR.bam

