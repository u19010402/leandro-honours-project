#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/scripts
#PBS -M u19010402@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/urophylla

gatk SelectVariants \
        -R /nlustre/users/leandro/2022/hons_project/data/urophylla_anneri.fasta \
        -V urophylla_output.vcf.gz \
        --select-type-to-include SNP \
        -O uro_raw_snp.vcf

echo 'SNP DONE'

gatk SelectVariants \
        -R /nlustre/users/leandro/2022/hons_project/data/urophylla_anneri.fasta \
        -V urophylla_output.vcf.gz \
        --select-type-to-include INDEL \
        -O uro_raw_indel.vcf

echo 'INDEL DONE'
