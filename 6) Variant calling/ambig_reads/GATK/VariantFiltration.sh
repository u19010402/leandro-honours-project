#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=00:30:00
#PBS -q short
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/scripts
#PBS -e /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/scripts
#PBS -M u19010402@tuks.co.za

module load gatk-4.2.6.1

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/grandis

gatk VariantFiltration \
        -V gra_raw_snp.vcf \
        -filter "QD < 2.0" --filter-name QD2 \
        -filter "QUAL < 30.0" --filter-name QUAL30 \
        -filter "SOR > 3.0" --filter-name SOR3 \
        -filter "FS > 60.0" --filter-name FS60 \
        -filter "MQ < 40.0" --filter-name MQ40 \
        -filter "MQRankSum < -12.5" --filter-name MQRankSum-12.5 \
        -filter "ReadPosRankSum < -8.0" --filter-name ReadPosRankSum-8 \
        -O gra_snps_filtered.vcf

echo "SNP DONE"

gatk VariantFiltration \
        -V gra_raw_indel.vcf \
        -filter "QD < 2.0" --filter-name QD2 \
        -filter "QUAL < 30.0" --filter-name QUAL30 \
        -filter "FS > 200.0" --filter-name FS200 \
        -filter "ReadPosRankSum < -20.0" --filter-name ReadPosRankSum-20 \
        -O gra_indels_filtered.vcf

echo "INDEL DONE"

echo "GRA DONE"

cd /nlustre/users/leandro/2022/fresh/REDO/AMBIG_REDO/GATK/urophylla

gatk VariantFiltration \
        -V uro_raw_snp.vcf \
        -filter "QD < 2.0" --filter-name QD2 \
        -filter "QUAL < 30.0" --filter-name QUAL30 \
        -filter "SOR > 3.0" --filter-name SOR3 \
        -filter "FS > 60.0" --filter-name FS60 \
        -filter "MQ < 40.0" --filter-name MQ40 \
        -filter "MQRankSum < -12.5" --filter-name MQRankSum-12.5 \
        -filter "ReadPosRankSum < -8.0" --filter-name ReadPosRankSum-8 \
        -O uro_snps_filtered.vcf

echo "SNP DONE"

gatk VariantFiltration \
        -V uro_raw_indel.vcf \
        -filter "QD < 2.0" --filter-name QD2 \
        -filter "QUAL < 30.0" --filter-name QUAL30 \
        -filter "FS > 200.0" --filter-name FS200 \
        -filter "ReadPosRankSum < -20.0" --filter-name ReadPosRankSum-20 \
        -O uro_indels_filtered.vcf

echo "INDEL DONE"

echo "URO DONE"
