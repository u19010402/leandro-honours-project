#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/repeatmasker
#PBS -e /nlustre/users/leandro/2022/hons_project/repeatmasker
#PBS -M email@tuks.co.za

cd /nlustre/users/leandro/2022/hons_project/repeatmasker/grandis

module load repeatmodeler

#specific consensi.fa file from RepeatModeler output

RepeatMasker -pa 28 -lib /nlustre/users/leandro/2022/hons_project/repeatmodeler/RM_14990.TueJul191035222022/consensi.fa -gff -a -noisy -xsmall /nlustre/users/leandro/2022/hons_project/data/haplogenomes/grandis_anneri.fasta -dir /nlustre/users/leandro/2022/hons_project/repeatmasker/grandis
