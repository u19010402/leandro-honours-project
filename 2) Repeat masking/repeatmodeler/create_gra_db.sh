#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/repeatmodeler
#PBS -e /nlustre/users/leandro/2022/hons_project/repeatmodeler
#PBS -M email@tuks.co.za

cd /nlustre/users/leandro/2022/hons_project/repeatmodeler/grandis_database

module load maker-2.31.10
module load repeatmodeler

BuildDatabase -name grandis_db /nlustre/users/leandro/2022/hons_project/data/haplogenomes/grandis_anneri.fasta
