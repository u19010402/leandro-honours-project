#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=120:00:00
#PBS -q long
#PBS -k oe
#PBS -m ae
#PBS -o /nlustre/users/leandro/2022/hons_project/repeatmodeler
#PBS -e /nlustre/users/leandro/2022/hons_project/repeatmodeler
#PBS -M email@tuks.co.za

cd /nlustre/users/leandro/2022/hons_project/repeatmodeler/

module load repeatmodeler

# create database before running RepeatModeler

RepeatModeler -pa 28 -database /nlustre/users/leandro/2022/hons_project/repeatmodeler/grandis_database/grandis_db -LTRStruct 
